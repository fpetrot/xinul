#pragma once

/* Wrapper sur les verrous basés sur les sémaphores ou les files de messages */
union sem {
	int fid;
	int sem;
};
void xwait(union sem *s);
void xsignal(union sem *s);
void xsdelete(union sem *s);
void xscreate(union sem *s);
