/*
 * Damien Dejean - Gaëtan Morin
 * Mathieu Barbe
 * Ensimag, Projet Système 2010
 *
 * XUNIL
 * Headers de la bibliothèque d'appels systèmes.
 */

#pragma once

#include "stdint.h"

/*
 * Gestion des processus
 */
int start(const char *name, unsigned long ssize, int prio, void *arg);
int getname(int pid, char *buffer, unsigned int size);
int getpid(void);
int getstatus(int pid, char *buffer, unsigned int size);
int getprio(int pid);
int chprio(int pid, int newprio);
int waitpid(int pid, int *retvalp);
int waitpid_nohang(int pid, int *retvalp);
int kill(int pid);
/*
void exit(int retval);
 * La fonction exit() est aussi déclarée dans stdlib.h mais c'est belle et bien un syscall à implémenter!
 */

/*
 * Gestion de la console
 */
void cons_echo(int on);
int cons_write(const char *str, long size);
int cons_wait(void);
unsigned long cons_read(char *string, unsigned long length);

/*
 * Gestion du temps
 */
void wait_clock(unsigned long clock);
void sleep(unsigned long sec);
unsigned long current_clock(void);
void sleepms(unsigned long ms);
void clock_settings(unsigned long *quartz, unsigned long *ticks);

/*
 * Gestion de la mémoire partagée
 */
void *shm_create(const char*);
void *shm_acquire(const char*);
void shm_release(const char*);

/*
 * Message
 */
int pcount(int fid, int *count);
int pcreate(int count);
int pdelete(int fid);
int preceive(int fid, int *message);
int preset(int fid);
int psend(int fid, int message);
int psize(int fid, int *size);

/*
 * Semaphore (En option)
 */
int scount(int sem);
int screate(short count);
int sdelete(int sem);
int signal(int sem);
int signaln(int sem, short count);
int sreset(int sem, short count);
int try_wait(int sem);
int wait(int sem);
