#pragma once

#include "xlock.h"
#include "syscall.h"

struct test11_shared {
        union sem sem;
        int in_mutex;
};
