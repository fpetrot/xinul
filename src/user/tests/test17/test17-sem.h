#pragma once

#include "syscall.h"

struct test17_buf_st {
    int mutex;
    int wsem;
    unsigned wpos;
    int rsem;
    unsigned rpos;
    char buf[100];
    int received[256];
};

// Increment a variable in a single atomic operation
static __inline__ void atomic_incr(int *atomic)
{
    __asm__ __volatile__("incl %0" : "+m" (*atomic) : : "cc");
}
